package com.example.springboot3demo.bean;

/**
 * @Description TODO
 * @Date 2023-11-30 7:56
 * @Created by yangjx
 */
public class User {
    String name;
    Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
