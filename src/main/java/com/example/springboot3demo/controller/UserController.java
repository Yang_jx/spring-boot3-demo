package com.example.springboot3demo.controller;

import com.example.springboot3demo.bean.User;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @GetMapping("/user")
    public String getUser(String name,Integer age) {

        return "GET-张三===" + name +"-" +age;
    }

    @PostMapping("/user")
    public User saveUser(@RequestBody User user) {
        return user;
    }

    @PutMapping("/user")
    public String putUser() {
        return "PUT-张三";
    }

    @DeleteMapping("/user")
    public String deleteUser() {
        return "DELETE-张三";
    }
}