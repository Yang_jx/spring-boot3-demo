package com.example.springboot3demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {
    @GetMapping("/index")
    public String hello(Model model) {
        model.addAttribute("msg", "hello !!!");
        return "index";
    }
}
